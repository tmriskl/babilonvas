import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

import java.io.*;
import java.util.*;


public class CreateVAS_JSON_ForBondles {
    public static String[]  fileNamesXLS = {"BabilonVas.xls","OffersUUIDs.xls"};

    public static HashMap<String,Set<Double>> prices = new HashMap<>();
    public static HashMap<String,Set<String>> feeTypes = new HashMap<>();
    public static HashMap<String,String> OfferNameToUUID = new HashMap<>();
    public static HashMap<String,List<String>> PlanNameToOffersNames = new HashMap<>();

    public static String    parameterUUID = "",
            rpSet = "02b7fac5-eb15-4f9f-a6cc-72613b8f7e07",
            layoutDescription = null,
            OneTime = "ONE_TIME", Monthly = "MONTHLY",
            PeriodicBalanceID = "8f3c0d16-bcaf-44c9-9143-e4994f029537", UsageBalanceID = "8f3c0d16-bcaf-44c9-9143-e4994f029537", productId = "44f325bb-186d-4b7d-a64b-adc7880da9c3";

    public static String
            offerStart =    "{\n" +
                            "  \"actionSetIds\": [\n" +
                            "  ],\n" +
                            "  \"cardinality\": 0,\n" +
                            "  \"compatibilitySetIds\": [\n" +
                            "  ],\n" +
                            "  \"customFields\": {\n" +
                            "  },\n" +
                            "  \"description\": \"\",\n" +
                            "  \"eligibilitySetIds\": [\n" +
                            "  ],\n" +
                            "  \"isDeleted\": false,\n" +
                            "  \"isIndividual\": false,\n" +
                            "  \"isOffered\": true,\n" +
                            "  \"isPrimary\": false,\n" +
                            "  \"limit\": null,\n" +
                            "  \"name\": \"",

            offerMiddle1 =  "\",\n" +
                            "  \"nestedProductOfferIds\": [\n" +
                            "  ],\n" +
                            "  \"offerTerm\": {\n" +
                            "    \"allowance\": 0,\n" +
                            "    \"currencyId\": \"TJS\",\n" +
                            "    \"effectiveDateFrom\": \"2021-01-01T07:12:09.240Z\",\n" +
                            "    \"offerPriority\": 0,\n" +
                            "    \"realm\": \"string\",\n" +
                            "    \"recurringType\": {\n" +
                            "      \"isCalendar\": true,\n" +
                            "      \"isProrated\": false,\n" +
                            "      \"realm\": \"\",\n" +
                            "      \"type\": \"",

            offerMiddle2=   "\",\n" +
                            "      \"value\": 0\n" +
                            "    },\n" +
                            "    \"tariffPlanId\": null,\n" +
                            "    \"type\": \"RATING\"\n" +
                            "  },\n" +
                            "  \"periodicBalanceId\": \""+PeriodicBalanceID+"\",\n" +
                            "  \"price\": {\n" +
                            "    \"currencyId\": \"TJS\",\n" +
                            "    \"realm\": \"string\",\n" +
                            "    \"value\": ",

            offerEnd=       "\n" +
                            "  },\n" +
                            "  \"productId\": \""+productId+"\",\n" +
                            "  \"realm\": \"\",\n" +
                            "  \"type\": \"SIMPLE_PRODUCT_OFFERING\",\n" +
                            "  \"usageBalanceId\": \""+UsageBalanceID+"\"\n" +
                                        "}",

            layoutStart =                       "{\n" +
            "    \"realm\": \"\",\n" +
            "    \"version\": 0,\n" +
            "    \"name\": \"",

    layoutMiddleBeforeDescription =     "\",\n" +
            "    \"description\":",

    layoutMiddleAfterDescription =      ",\n" +
            "    \"data\": [",

    layoutEnd =                         "],\n" +
            "    \"ratingParameterSetId\": \""+rpSet+"\",\n" +
            "    \"isIndividual\": false,\n" +
            "    \"isDeleted\": false\n" +
            "}",


    tariffStart =                       "\n{\n" +
            "            \"tariffId\": \"",

    tariffMiddle1 =                     "\",\n" +
            "            \"`list`\": [\n" +
            "                {\n" +
            "                    \"calendarPropertiesId\": null,\n" +
            "                    \"id\": \"",

    tariffMiddle2 =                     "\",\n" +
            "                    \"position\": ",

    tariffEnd =                         "\n" +
            "                }\n" +
            "            ]\n" +
            "        },",


    groupInLayoutStart =                "\"parameterGroupName\":\"",

    groupInLayoutMiddle =               "\",\n" +
            "              \"value\": [\n",

    groupInLayoutEnd =                  "              \n]\n" +
            "            },\n" +
            "            {\n" +
            "              \"parameterGroupName\": null,\n" +
            "              \"value\": [",


    startGroup =                        "{\n" +
            "        \"realm\": \"\",\n" +
            "        \"name\": \"",

    middleGroup =                       "\",\n" +
            "        \"description\": \"\",\n" +
            "        \"values\": [\n",

    endGroup =                          "\n" +
            "        ],\n" +
            "        \"parameters\": ["+parameterUUID+"],\n" +
            "        \"isDeleted\": false\n" +
            "    }",


    layoutRowStart =                    "\n" +
            "        {\n" +
            "            \"position\":",

    layoutRowMiddle1 =                  ",\n" +
            "            \"parameters\": [\n" +
            "                {",

    layoutRowMiddle2 =                  "\n" +
            "                    ]\n" +
            "                }\n" +
            "            ],\n" +
            "            \"tags\": [",

    layoutRowEnd =                      "\n" +
            "            ],\n" +
            "            \"group\": null,\n" +
            "            \"isDeleted\": false\n" +
            "        },";




    public static void main(String[] args) {
        Date start = new Date();
        System.out.println(start);
        System.out.println();
        System.out.println();

        try {
            GetOffersUUIDs();
            GetServicesPrices();
            PrintOfferingToVasJson();
        } catch (IOException ignore) {

        }

        System.out.println();
        System.out.println();
        Date end = new Date();
        System.out.println(end);
        System.out.println("Running took " + (end.getTime() - start.getTime())/1000.0 + " Seconds");
    }

    private static void GetOffersUUIDs() throws IOException {
        FileInputStream fis = new FileInputStream(new File(fileNamesXLS[1]));
        HSSFWorkbook workbook = new HSSFWorkbook(fis);
        List<HSSFSheet> sheets = new LinkedList<>();
        for(int i = 0;i<workbook.getNumberOfSheets();i++)
            sheets.add(workbook.getSheetAt(i));

        HSSFSheet sheetGroups = sheets.get(0);
        for(int i = 1 ; i< sheetGroups.getPhysicalNumberOfRows(); i++) {
            Row row = sheetGroups.getRow(i);
            String name = row.getCell(0).getStringCellValue();
            String uuid = row.getCell(1).getStringCellValue();
            OfferNameToUUID.put(name,uuid);

        }

    }

    private static void PrintOfferingToVasJson() throws FileNotFoundException {
            StringBuilder plans = new StringBuilder("{");
            for (Map.Entry<String, List<String>> entry : PlanNameToOffersNames.entrySet()) {
                String PlanName = entry.getKey();
                StringBuilder plan = new StringBuilder("{");
                Map<String, List<String>> serviceToOffers = new HashMap<>();
                for(String offerName : entry.getValue()) {
                    String ServiceName = offerName.split("_")[0];
                    serviceToOffers.computeIfAbsent(ServiceName, k -> new LinkedList<>()).add(OfferNameToUUID.get(offerName));
                }

                for(String ServiceName: serviceToOffers.keySet()) {
                    StringBuilder uuids = new StringBuilder("[");
                    for(String uuid: serviceToOffers.get(ServiceName)){
                        uuids.append("\"").append(uuid).append("\",");
                    }
                    String uuidsString = uuids.substring(0, uuids.toString().length() - 1) + "]";
                    plan.append("\n\"").append(ServiceName).append("\":")
                            .append(uuidsString).append(",");
                }

                PrintWriter pw = new PrintWriter(new File("Plans//"+PlanName + " Bundle.txt"));
                String planString = plan.substring(0, plan.toString().length() - 1) + "}";
                pw.println(planString);
                pw.close();
                plans.append("\n\"").append(PlanName).append("\"").append(":").append(planString).append(",");
                System.out.println(PlanName + " Bundle");
            }
            PrintWriter pw = new PrintWriter(new File("Plans//0_All.txt"));
            pw.println(plans.substring(0, plans.toString().length() - 1) + "}");
            pw.close();




    }

    private static void GetServicesPrices() throws IOException {
        FileInputStream fis = new FileInputStream(new File(fileNamesXLS[0]));
        HSSFWorkbook workbook = new HSSFWorkbook(fis);
        List<HSSFSheet> sheets = new LinkedList<>();
        for(int i = 0;i<workbook.getNumberOfSheets();i++)
            sheets.add(workbook.getSheetAt(i));

        HSSFSheet sheetGroups = sheets.get(0);
        Row row0 = sheetGroups.getRow(0);


        for(int i = 1 ; i< sheetGroups.getPhysicalNumberOfRows(); i++) {
            Row row = sheetGroups.getRow(i);
            try {
                String  ServiceName = row.getCell(0).getStringCellValue().trim();

                prices.computeIfAbsent(ServiceName, k -> new HashSet<>());
                if(ServiceName != null&&!ServiceName.equals("")) {
                    double type = row.getCell(1).getNumericCellValue();
                    Set<String> typeStrings = new HashSet<>();
                    if (type > 0)
                        typeStrings.add(Monthly);
                    type = row.getCell(2).getNumericCellValue();
                    if (type > 0)
                        typeStrings.add(OneTime);

                    for (int j = 3; j < row.getLastCellNum(); j++) {
                        double price = -1;
                        try {
                            price = row.getCell(j).getNumericCellValue();
                        } catch (IllegalStateException e) {
                            try {
                                price = Double.parseDouble(row.getCell(j).getStringCellValue());
                            } catch (Exception ignore) {

                            }
                        }
                        if ((price != -1)&&(!typeStrings.isEmpty())) {
                            prices.get(ServiceName).add(price);
                            feeTypes.put(ServiceName, typeStrings);
                            for(String typeString: typeStrings) {
                                String offerName = ServiceName + "_" + price + "_" + typeString,
                                        planName = row0.getCell(j).getStringCellValue();
                                PlanNameToOffersNames.computeIfAbsent(planName, k -> new LinkedList<>());
                                PlanNameToOffersNames.get(planName).add(offerName);
                            }
                        }
                    }
                }
            }catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

//        for(Map.Entry entry: prices.entrySet()) {
//            System.out.println(entry.getKey());
//            System.out.println(entry.getValue().toString());
//        }
//        System.out.println(prices.size());


    }

}





































