{
  "actionSetIds": [
  ],
  "cardinality": 0,
  "compatibilitySetIds": [
  ],
  "customFields": {
  },
  "description": "",
  "eligibilitySetIds": [
  ],
  "isDeleted": false,
  "isIndividual": false,
  "isOffered": true,
  "isPrimary": false,
  "limit": null,
  "name": "Static IP-address (23.6 у.е.)_81.0152_ONE_TIME",
  "nestedProductOfferIds": [
  ],
  "offerTerm": {
    "allowance": 0,
    "currencyId": "TJS",
    "effectiveDateFrom": "2021-01-01T07:12:09.240Z",
    "offerPriority": 0,
    "realm": "string",
    "recurringType": {
      "isCalendar": true,
      "isProrated": false,
      "realm": "",
      "type": "ONE_TIME",
      "value": 0
    },
    "tariffPlanId": null,
    "type": "RATING"
  },
  "periodicBalanceId": "8f3c0d16-bcaf-44c9-9143-e4994f029537",
  "price": {
    "currencyId": "TJS",
    "realm": "string",
    "value": 81.0152
  },
  "productId": "44f325bb-186d-4b7d-a64b-adc7880da9c3",
  "realm": "",
  "type": "SIMPLE_PRODUCT_OFFERING",
  "usageBalanceId": "8f3c0d16-bcaf-44c9-9143-e4994f029537"
}
