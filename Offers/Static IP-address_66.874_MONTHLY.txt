{
  "actionSetIds": [
  ],
  "cardinality": 0,
  "compatibilitySetIds": [
  ],
  "customFields": {
  },
  "description": "",
  "eligibilitySetIds": [
  ],
  "isDeleted": false,
  "isIndividual": false,
  "isOffered": true,
  "isPrimary": false,
  "limit": null,
  "name": "Static IP-address_66.874_MONTHLY",
  "nestedProductOfferIds": [
  ],
  "offerTerm": {
    "allowance": 0,
    "currencyId": "TJS",
    "effectiveDateFrom": "2021-01-01T07:12:09.240Z",
    "offerPriority": 0,
    "realm": "string",
    "recurringType": {
      "isCalendar": true,
      "isProrated": false,
      "realm": "",
      "type": "MONTHLY",
      "value": 0
    },
    "tariffPlanId": null,
    "type": "RATING"
  },
  "periodicBalanceId": "8f3c0d16-bcaf-44c9-9143-e4994f029537",
  "price": {
    "currencyId": "TJS",
    "realm": "string",
    "value": 66.874
  },
  "productId": "44f325bb-186d-4b7d-a64b-adc7880da9c3",
  "realm": "",
  "type": "SIMPLE_PRODUCT_OFFERING",
  "usageBalanceId": "8f3c0d16-bcaf-44c9-9143-e4994f029537"
}
